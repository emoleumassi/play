/*
 * Copyright 2017 IQTIG – Institut für Qualitätssicherung und Transparenz im Gesundheitswesen.
 * Diese Code ist urheberrechtlich geschützt (Copyright). Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet, beim IQTIG.
 * Wer gegen das Urheberrecht verstößt, macht sich gem. § 106 ff Urhebergesetz strafbar. Er wird zudem kostenpflichtig abgemahnt und muss
 * Schadensersatz leisten.
 */

/**
 * @author emo.leumassi
 */
public class Fruit {

	private int id;
	private String name;
	private boolean bla;

	public Fruit(int id, String name) {
		System.setProperty("Emo", "Leumassi");
		this.id = id;
		this.name = name;
	}

	public Fruit(final int id, final String name, final boolean bla) {
		this.id = id;
		this.name = name;
		this.bla = bla;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isBla() {
		return bla;
	}

	public void setBla(final boolean bla) {
		this.bla = bla;
	}

	@Override
	public String toString() {
		return "Fruit{" +
			"id=" + id +
			", name='" + name + '\'' +
			", bla=" + bla +
			'}';
	}
}
