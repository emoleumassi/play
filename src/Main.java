import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.IntSummaryStatistics;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	private final static AtomicInteger fussnoteCounter = new AtomicInteger(0);

	public class MyComparator implements Comparator<String> {

		@Override
		public int compare(String s1, String s2) {
			if (s1.contains(":") && s2.contains(":")) {
				Integer i1 = Integer.valueOf(s1.split(":")[0]);
				Integer i2 = Integer.valueOf(s2.split(":")[0]);
				return i1.compareTo(i2);
			}
			return s1.compareTo(s2);
		}
	}

	public void testFindFirst() {
		List<Fruit> fruitList = Arrays.asList(new Fruit(1, "banane"), new Fruit(2, "orange"));
		Fruit fruit = fruitList.stream().findFirst().get();
		System.out.println(fruit.toString());
	}

	public void testMap() {
		List<Fruit> fruitList = Arrays.asList(new Fruit(1, "banane"), new Fruit(2, "orange"));
		List<Integer> fruitIds = fruitList.stream().map(Fruit::getId).collect(Collectors.toList());
		fruitIds.forEach(System.out::println);
		System.out.println("fruitList" + fruitList.toString());
		System.out.println("fruitIds" + fruitIds.toString());
	}

	public void testMap2() {
		List<Fruit> fruitList = Arrays.asList(new Fruit(1, "banane"), new Fruit(2, "orange"), new Fruit(3, "orange"), new Fruit(4, "orange"));
		List<Integer> fruitIds = fruitList.stream().map(Fruit::getId).filter(id -> id >= 3).collect(Collectors.toList());
		fruitIds.forEach(System.out::println);
	}

	public void testMap3() {
		List<Fruit> fruitList = Arrays.asList(new Fruit(1, "banane"), new Fruit(2, ""), new Fruit(3, "Ananas"), new Fruit(4, ""));
		List<String> fruitIds = fruitList.stream().map(Fruit::getName).filter(name -> !name.isEmpty()).collect(Collectors.toList());
		fruitIds.forEach(System.out::println);
	}

	public void testMap4() {
		List<Fruit> fruitList = Arrays.asList(new Fruit(1, "banane"), new Fruit(2, "Tomate"), new Fruit(3, "Ananas"), new Fruit(4, "Carott"));
		fruitList.stream().map(fruit -> {
			fruit.setName("Cerise");
			return fruit;
		}).forEach(System.out::println);
	}

	public void testMap5() {
		List<Fruit> fruitList = Arrays.asList(new Fruit(1, "banane"), new Fruit(2, "Tomate"), new Fruit(3, "Ananas"), new Fruit(10, "Carott"));
		int number = fruitList.stream().anyMatch(fruit -> fruit.getId() == 3) ? 0 : 3;
		System.out.println("the found number is: " + number);
	}

	public void testIterate(int start) {
		Stream.iterate(start, n -> n + n).limit(10).forEach(System.out::println);
	}

	public void testCollect() {
		List<Fruit> fruitList = Arrays.asList(new Fruit(1, "banane"), new Fruit(2, "Poivre"), new Fruit(3, "Ananas"), new Fruit(4, "Mango"));
		Map<String, List<Fruit>> listMap = fruitList.stream().collect(Collectors.groupingBy(Fruit::getName));
		System.out.println(listMap.toString());
	}

	public void testStringReplace() {
		String test = "(appel moi)";
		test = ", " + test.replaceAll("[()]", "");
		System.out.println(test);
	}

	public void testDigits() {
		float f = 0.645564f;
		double tmp = Math.round(f * 100);
		String str2 = new BigDecimal(tmp)
			.setScale(2, BigDecimal.ROUND_UP)
			.toString();
		System.out.println(str2);
		System.out.println(str2.replace('.', ','));
	}

	public <T> void getClassName(Class<T> tClass) {
		System.out.println("This class's name: " + tClass.getName());
	}

	public void testDigits2() {
		System.out.println(String.format("%.0f", 90.09));
	}

	public void testIntSummaryStatistic() {
		List<Fruit> fruitList = Arrays.asList(new Fruit(123, "B"), new Fruit(24, "P"), new Fruit(30, "A"), new Fruit(56, "M"));
		IntSummaryStatistics intSummaryStatistics = fruitList.stream().collect(Collectors.summarizingInt(Fruit::getId));
		System.out.println(intSummaryStatistics.toString());
	}

	public void testStringJoin() {
		List<String> stringList = Arrays.asList("Viande", "Poisson", "Canard", "Sardine", "Cerise", "Pigeon");
		String result = stringList.stream().collect(Collectors.joining(", "));
		System.out.println(result);
	}

	public void testDate() {
		Date date = new Date();
		System.out.println(date.toString());
	}

	public void testFieldReflection() {
		TestField testField = new TestField(fussnoteCounter);
		Field[] fields = testField.getClass().getDeclaredFields();
		Arrays.stream(fields).forEach(field -> System.out.println(field.getName()));
	}

	public void testUnaryOperator() {
		List<Integer> list = Arrays.asList(10, 20, 30, 40, 50);
		UnaryOperator<Integer> unaryOpt = i -> i * i;
		list.forEach(i -> System.out.println(unaryOpt.apply(i)));
	}

	public void testCompareTo() {
		String a = "4100125";
		String b = "412";

		System.out.println(a.toLowerCase().compareTo(b));

		List<String> tmp = Arrays.asList("1", "1.2", "500", "400", "41", "2", "5", "2.5");
		tmp.sort(String::compareTo);
		System.out.println(tmp);
	}

	public void testImanRegEx() {
		String NON_ALPHANUMERIC = "\\d+";
		if ("1.5".matches(NON_ALPHANUMERIC)) {
			System.out.println("Iman hatte Recht");
		}
	}

	public void testReplace() {
		String text = "&amp; round(difftimeAsNumeric(GEBDATUMK, AUFNDATUM, unit=\"days\")) &gt;= 2";
		String result = text.replaceAll("&amp;", "&");
		result = result.replaceAll("&gt;", ">");
		System.out.println(result);
	}

	private void testCreateTempDir() {
		try {
			Path path = Files.createTempDirectory("QIDB");
			System.out.println(path.toString());
		} catch (IOException e) {

		}
	}

	private void testFunction(Function<Integer, Boolean> function, Integer integer) {
		Boolean booleanTest = function.apply(integer);
		System.out.println(booleanTest);
	}

	private void testFunction1(Function<String, Integer> function, String tmp) {
		Integer integer = function.apply(tmp);
		System.out.println(integer);
	}

	private void testConsumer(List<String> list, Consumer<String> consumer) {
		list.forEach(s -> consumer.accept(s));
	}

	private void testContinue() {
		List<String> stringList = Arrays.asList("Emo", "Leumassi", "Test", "Huhu");
		for (String test : stringList) {
			if ("Leumassi".equals(test)) {
				continue;
			}
			System.out.println(test);
		}
	}

	private void testOptional(String me) {
		Optional<String> text = Optional.ofNullable(me);
		text.ifPresent(currentText -> System.out.println("huhu " + me));
	}

	private void testOptional1(String me) {
		Optional<String> text = Optional.ofNullable(me);
		System.out.println("bla bla " + text);
	}

	private void testOptional2() {
		Optional<String> text = Optional.empty();
		text.map(s -> "huhu").toString();
		System.out.println(text);
	}

	private void testOptional3() {
		Optional<String> text = Optional.ofNullable("Demain ");
		text.map(s -> "huhu").toString();
		System.out.println(text.get());
	}

	private void testMap1() {
		Map<String, String> stringMap = new HashMap<>();
		stringMap.put("Emo", "Hallo");
		stringMap.put("Ferdinand", "Frederic");
		stringMap.put("T", "E");
		stringMap.entrySet().forEach(stringStringEntry -> System.out.println(stringStringEntry.getValue()));
	}

	private void testMap0() {
		Map<String, String> stringMap = new HashMap<>();
		stringMap.put("Emo", null);
		stringMap.put("Ferdinand", "Frederic");
		stringMap.put("T", "E");

		stringMap.entrySet().stream()
			.filter(stringStringEntry -> Objects.equals(stringStringEntry.getValue(), null))
			.findAny()
			.ifPresent(stringStringEntry -> {
				throw new IllegalArgumentException("Die Spalteeeeeeeeeeeeeeeee " + stringStringEntry.getKey() + " muss einen Wert haben haben!");
			});
	}

	private void testEnum() {
		List<String> stringList = Arrays.asList("ad34dsa", "dk4329", "AD4943", "DE49343", "EDdfj", "frHERNK", "FRjeje", "ZE3u93");
		EnumTest[] enumTests = EnumTest.values();
		for (EnumTest enumTest : enumTests) {
			List<String> stringList1 = stringList.stream()
				.filter(s -> s.toUpperCase().startsWith(enumTest.toString()))
				.map(String::toString)
				.collect(Collectors.toList());
			System.out.println(stringList1.toString());
		}
	}

	private void testReduce() {
		Stream.of(23, 43, 56, 97, 32).reduce(Integer::max).ifPresent(System.out::println);
	}

	private void testReduce1() {
		Stream.of(23, 43, 56, 97, 32).reduce(Integer::sum).ifPresent(System.out::println);
	}

	private void testSum() {
		Integer i = Stream.of(23, 43, 56, 97, 32).mapToInt(Integer::intValue).sum();
		System.out.println(i);
	}

	private void testPeek() {
		Stream.of("one", "two", "three", "four")
			.filter(e -> e.length() > 3)
			.peek(e -> System.out.println("Filtered value: " + e))
			.map(String::toUpperCase)
			.peek(e -> System.out.println("Mapped value: " + e))
			.collect(Collectors.toList());
	}

	private enum EnumTest {
		AD, DE, ZE, FR
	}

	private void testMapPut() {
		String[][] values = {
			{"%", "0,0", "50,0", "15,0", "true", "6,47", "5,19", "12,33"},
			{"%", "0,0", "50,0", "15,0", "true", "9,36", "8,94", "12,33"},
			{"%", "0,0", "50,0", "15,0", "true", "17,21", "16,48", "15,22"},
			{"%", "0,0", "50,0", "15,0", "true", "9,96", "11,65", "0,00"},
			{"%", "1,0", "3,5", "1,7", "true", "2,30", "2,22", "-"},
			{"%", "0,0", "50,0", "15,0", "true", "6,47", "5,22", "13,33"},
			{"%", "0,0", "50,0", "15,0", "true", "9,37", "8,94", "13,33"},
			{"%", "0,0", "50,0", "30,0", "true", "17,23", "16,47", "15,22"},
			{"%", "0,0", "100,0", "", "true", "59,36", "59,28", "57,69"},
			{"Jahre", "0,0", "100,0", "", "true", "71,00", "69,00", "71,00"},
		};
		List<Fruit> fruits = new LinkedList<>();
		Map<String, List<Fruit>> stringListMap = new HashMap<>();
		for (int i = 0; i < values.length; i++) {
			Fruit fruit = new Fruit(i, values[i][5]);
			fruits.add(fruit);
			if (i <= 3) {
				stringListMap.put("Auffälligkeitskriterium", fruits);
				if (i == 3) {
					fruits = new LinkedList<>();
				}
			} else {
				stringListMap.put("Deskriptive Statistik", fruits);
			}
		}

		stringListMap.forEach((s, fruits1) -> System.out.println("Key: " + s + ", value: " + fruits1));
	}

	private void reserveList(List<String> strings) {
		/*List<String> revList = new ArrayList<>();
		Stream.of("je", "tu", "il", "elle", "on").forEach(s -> revList.add(0, s));*/
		//Stream.of("je", "tu", "il", "elle", "on").collect(Collectors.toCollection(LinkedList::new)).descendingIterator().forEachRemaining(System.out::println);
		//revList.forEach(System.out::println);
		Collections.reverse(strings);
		//revList.forEach(System.out::println);
	}

	private String a;
	private Boolean bBoolean = false;

	private String tmp(Boolean aBoolean, Consumer<Boolean> consumer) {
		if (aBoolean == null) {
			return "-";
		}
		this.bBoolean = aBoolean; //bBoolean = aBoolean;
		return this.bBoolean ? "ja" : "nein";
	}

	public void testSplit() {
		String temo = "9-17AP4-R6ET87VRLVHWBM";
		String index = temo.split("-", 2)[0];
		String index1 = temo.split("-", 2)[1];
		String index2 = temo.split("-", 2)[2];
		System.out.println(index);
	}

	public boolean testPattern() {
		Pattern pattern = Pattern.compile("a*b");
		return pattern.matcher("b").find();
	}

	public List<String> testPattern1(Pattern pattern) {
		return Stream.of("Emo", "Leumassi", "Test", "Huhu")
			.filter(s -> pattern != null && pattern.matcher(s).matches())
			.collect(Collectors.toList());
	}

	private boolean testAnyMatch() {
		Fruit fruit = new Fruit(1, "2017");
		return Stream.of("2017", "2015", "2017", "2016", "2014")
			.anyMatch(s -> s.equals(fruit.getName()));
	}

	private void testDateFormat() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss yyyy", Locale.GERMAN);
		LocalDateTime dateTime = LocalDateTime.parse("Mi Mai 09 09:26:18 2018", dtf);
		DateTimeFormatter df = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		System.out.println(df.format(dateTime));

		System.out.println(LocalDateTime.now().format(DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss yyyy", Locale.GERMAN)));
		System.out.println(LocalDate.now(ZoneId.systemDefault()).toString());

		LocalDate localDate = LocalDateTime
			.parse(LocalDateTime.now().format(DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss yyyy", Locale.GERMAN)), DateTimeFormatter
				.ofPattern("EEE MMM dd HH:mm:ss yyyy", Locale.GERMAN)).toLocalDate();
		System.out.println();

		/*LocalDate _now=LocalDate.now();
		// DateTimeFormatter.ofPattern(PATTTERN_DATE, LOCALE )
		Arrays.asList(Locale.getAvailableLocales()).
			forEach(locale->{
				System.out.println("Locale "+locale.getDisplayCountry()
					+"\t Print Date \t: "
					+_now.format(DateTimeFormatter.ofPattern("EEEE , dd MMMM yyyy", locale)));
			});*/
	}

	private void testReversed() {
		Stream.of(new Fruit(1, "banane", false), new Fruit(1, "banane", true), new Fruit(2, "Ananas", true))
			.sorted(Comparator.comparing(Fruit::getId).thenComparing(Fruit::isBla).thenComparing(Fruit::getName))
			.forEach(System.out::println);
		System.out.println();
		Stream.of(new Fruit(1, "banane", true), new Fruit(1, "banane", false), new Fruit(2, "Ananas", true))
			.sorted(Comparator.comparing(Fruit::getId).reversed().thenComparing(Fruit::isBla, Comparator.reverseOrder())
				.thenComparing(Fruit::getName))
			.forEach(System.out::println);
	}

	private void testBoolean() {
		boolean isValid = true;
		for (boolean b : Arrays.asList(true, false, true)) {
			System.out.println("Avant: " + b);
			isValid = isValid && b;
			System.out.println("Apres: " + isValid);
		}
		System.out.println("Finish: " + isValid);
	}

	private void testContainsWhiteSpaces() {
		final String s = "QSD_D_2018-Q1_LEAW_46DL00104_2018-06-06";
		Pattern pattern = Pattern.compile("\\s");
		Matcher matcher = pattern.matcher(s);
		System.out.println(matcher.find());
	}

	private void testListAddIndex() {
		List<String> unitList = Arrays.asList("Emo", "Leumassi");
		List<String> unitList1 = new ArrayList<>(unitList);
		unitList1.add(0, "Tchou");
		unitList1.add(0, "Bund");
		unitList1.forEach(System.out::println);
	}

	private void testDoubleFilter() {
		List<Fruit> fruitList = Arrays.asList(new Fruit(1, "banane"), new Fruit(2, "Poivre"), new Fruit(3, "Ananase"), new Fruit(4, "Mangoe"));
		fruitList.stream()
			.filter(fruit -> fruit.getName().contains("e"))
			.filter(fruit -> fruit.getName().contains("a"))
			.forEach(System.out::println);

		System.out.println("Bla");

		fruitList.stream()
			.filter(fruit -> fruit.getName().contains("e")
				&& fruit.getName().contains("a"))
			.forEach(System.out::println);
	}

	private void testRegex() {
		final String regex = "^QSD_D_%d-Q%d_LEAW_[-_A-Za-z0-9 ]{4,30}_\\d{4}-\\d{2}-\\d{2}$";
		final String reportName = "QSD_D_2018-Q1_LEAW_72 DQS 130_2018-06-14";
		final Pattern pattern = Pattern.compile(String.format(regex, 2018, 1));
		if (!pattern.matcher(reportName).matches()) {
			System.out.println("Don't match");
		}
		System.out.println("Match");
		/*String text = "ba#lkg#06awcpwt3ias-01";//-00
		String regex = "^[A-Z]{2}#(KV|LKG)#[A-Z0-9]{1,15}([-]\\d{2})?$";
		String tmp[] = text.split("#");
		System.out.println(text.toUpperCase().matches(regex));*/
	}

	private void testLocaleDate() {
		//yyyyMMdd_HHmm
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmm");
		System.out.println(LocalDateTime.now(ZoneId.of("Europe/Berlin")).format(formatter));
		System.out.println(LocalDateTime.now(ZoneId.of("America/New_York")).format(formatter));
		System.out.println(LocalTime.now().getMinute());
	}

	private void testLinkedList() {
		LinkedList<Integer> integers = new LinkedList<>();
		integers.addFirst(6);
		integers.addFirst(2);
		integers.addFirst(3);
		integers.add(0, 45);
		integers.add(1, 5);
		System.out.println(integers.toString());
	}

	private void testException() throws ArithmeticException {
		try {
			System.out.println(3 / 0);
		} catch (ArithmeticException e) {
			System.out.println("Fehler ");
		}
	}

	private void testException1() throws ArithmeticException {
		System.out.println(3 / 0);
	}

	private void testFormatNumber() {
		System.out.println(NumberFormat.getNumberInstance(Locale.GERMAN).format(999999));
	}

	private void testEmptyList() {
		List<String> integers = Collections.emptyList();
		integers.stream().map(Integer::valueOf).forEach(System.out::print);
		;
	}

	private void testSortLexical() {
		List<String> list = Arrays.asList("EF*", "12:B", "-", "1:A", "111:K");
		//List<String> list = Arrays.asList("12", "1", "111");
		list.stream()
			.sorted((s1, s2) -> new MyComparator().compare(s1, s2))
			.forEach(System.out::println);
		//System.out.println(list);
	}

	private void testDoubleValue() {

		final Double d = 12.0d;
		final String result;
		if (d % 1 == 0) {
			result = String.valueOf(d.intValue());
		} else {
			result = String.valueOf(d);
		}
		System.out.println(result);
	}

	private void testNumberFormat() {
		double val1 = 2;
		double val2 = 2.30;
		NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		nf.setMinimumFractionDigits(0);
		System.out.println(nf.format(val1));
		System.out.println(nf.format(val2));

		System.out.println(Double.valueOf(nf.format(val1)).intValue());
		System.out.println(Double.valueOf(nf.format(val2)).intValue());

		NumberFormat formatter = new DecimalFormat("#0.#");
		System.out.println(formatter.format(34.0));

		System.out.println(Double.toString(12.4d).indexOf('.'));
		System.out.println(Double.valueOf(12d).toString().split("\\.")[1]);
		System.out.println(new DecimalFormat("0.##").format(12.1d));
	}

	private void testTabulator() {
		System.out.println("Main\tdemain");
	}

	private void testEmptyCharachter() {
		System.out.println("5-541.1%".replaceAll("-", "\u2011"));
	}

	private void testSortStringWithSlash() {
		List<String> list = Arrays.asList("18/1", "HTXM-TX", "09/1", "15/1", "RST", "DEK", "HTXM-D");
		Collections.sort(list);
		System.out.println(list);
	}

	private void testComputeIfAbsent() {
		Map<String, String> map = new HashMap<>();
		map.computeIfAbsent("snoop", k -> "Hallo 1");
		map.computeIfAbsent("snoop1", k -> "Hallo 2");
		System.out.println(map.toString());
	}

	private void testCollectorsToMap() {
		List<Fruit> fruitList = Arrays.asList(
			new Fruit(1, "A"),
			new Fruit(1, "A"),
			new Fruit(3, "C"),
			new Fruit(4, "D")
		);
		//, (x, y) -> y
		Map<Integer, String> map = fruitList.stream().collect(Collectors.toMap(Fruit::getId, Fruit::getName, (x, y) -> x));
		System.out.println(map.toString());
	}

	private void testCompareBoolean() {
		Stream.of(true, false, true, false).sorted(Comparator.reverseOrder()).forEach(s -> System.out.println(s));
	}

	private void testCompareCaseSentive() {
		List<String> strings = Arrays.asList("ABC", "abc", "b", "B");
		strings.forEach(System.out::println);
		System.out.println("Emo");
		strings.stream().sorted(Comparator.comparing(String::toString, String.CASE_INSENSITIVE_ORDER)).forEach(System.out::println);
	}

	private void testAtomicInteger() {
		System.out.println(fussnoteCounter.incrementAndGet());
		System.out.println("Bla bla");
		System.out.println(fussnoteCounter.incrementAndGet());
		new TestField(fussnoteCounter);
	}

	private void testCompareThenComparing() {
		List<Fruit> fruitList = Arrays.asList(
			new Fruit(1, "A"),
			new Fruit(4, "D"),
			new Fruit(3, "C"),
			new Fruit(1, "A")
		);
		System.out.println(fruitList.toString());
		fruitList = fruitList.stream().sorted(Comparator.comparing(Fruit::getName).thenComparing(Fruit::getId)).collect(Collectors.toList());
		System.out.println(fruitList.toString());
	}


	private void testPredicateAnd() {
		final List<Fruit> fruitList = Arrays.asList(new Fruit(1, "AA"), new Fruit(2, "DD"),
			new Fruit(3, "CC"), new Fruit(4, "BB"));
		Predicate<Fruit> predicate = s -> s.getName().equals("AA");
		predicate = predicate.and(s -> s.getId() == 1);
		System.out.println(fruitList.stream().anyMatch(predicate));
	}

	public void testInstanceOf(final Object o) {
		if (o instanceof Fruit) {
			System.out.println("instanceof fruit " + ((Fruit) o).getName());
		} else {
			System.out.println("no");
		}
	}

	public void testOffSetDateTime(){
		System.out.println(OffsetDateTime.parse("2011-10-05T14:48:00.000Z"));
	}

	public void testOffSetDateTimeTruncate() {
		final OffsetDateTime time = OffsetDateTime.parse("2011-10-05T14:48:00.000Z");
		System.out.println(time);
		System.out.println(time.truncatedTo(ChronoUnit.DAYS));
	}

	public void testOffSetDateTimeToString() {
		final OffsetDateTime time = OffsetDateTime.parse("2011-10-05T14:48:00.000Z");
		System.out.println(time.toLocalDate().toString());
	}

	public void testReplaceFirst() {
		System.out.println("001".replaceFirst("^0+(?!$)", ""));
	}

	public void testPatternMatches(){
		System.out.println(Pattern.matches( "^[S]\\d{7}$", "S1111111"));
	}

	public void givenUsingApache_whenGeneratingRandomStringBounded_thenCorrect() {

		final int length = 29;

		final boolean useLetters = true;

		final boolean useNumbers = false;

		System.out.println(RandomStringUtils.random(length, useLetters, useNumbers));

		System.out.println(RandomStringUtils.random(length, useLetters, useNumbers));

		System.out.println(RandomStringUtils.random(length, useLetters, useNumbers));

		System.out.println(RandomStringUtils.random(length, useLetters, useNumbers));
	}

	public static void main(String[] args) {

		Main main = new Main();
		main.testReplaceFirst();
		/*SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		final String t = format.format(new Date());
		System.out.println(t);*/

		//main.testPredicateAnd();
		//System.out.println("".isEmpty());

		//System.out.println(new Date());

		/*System.out.println("Blu blu");
		System.out.println(fussnoteCounter.incrementAndGet());
		System.out.println(new StringBuilder(" ").toString().isEmpty());*/

		//Stream.of("A", "B").filter(null).forEach(System.out::println);

		/*DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.GERMAN);
		decimalFormat.applyPattern("0.00000000");//########
		System.out.println(decimalFormat.format(31.25));*/

		//System.out.println(main.testAnyMatch());

		/*BigDecimal b = new BigDecimal("0.232343");
		System.out.println(b.scaleByPowerOfTen(2).floatValue());*/

		/*System.out.println(main.testPattern());

		main.testPattern1(null).forEach(System.out::println);

		String t = null;

		String [] ts = t.split("-");
		System.out.println(ts[0]);


		main.a = main.tmp(false, c -> main.bBoolean = c);
		System.out.println(main.a);
		System.out.println(main.bBoolean);*/



		/*Fruit fruit = new Fruit(1, "");
		//main.testMapPut();
		Optional<Fruit> fruitOptional = Optional.of(fruit);
		System.out.println(fruitOptional.isPresent());

		System.out.println(LocalDate.now().getYear());
		System.out.println(System.getProperty("Emo"));

		System.out.println(new BigDecimal("12.3441").setScale(2, RoundingMode.HALF_UP));
		List<String> strings = Arrays.asList("je", "tu", "il", "elle", "on");
		main.reserveList(strings);
		strings.forEach(System.out::println);
		main.testOptional3();
		main.testOptional1(null);
		main.testMap1();
		main.testMap0();
		main.testReplace();
		main.testContinue();
		main.testFunction(t -> t == 2, 2);
		main.testFunction1(t -> "test".equals(t) ? 2 : 5, "Hello world");
		main.testConsumer(Arrays.asList("a", "b", "c", "d"), s -> System.out.println(s));
		main.testCreateTempDir();
		main.testImanRegEx();
		main.testCompareTo();
		main.testUnaryOperator();
		main.testMap();
		main.testMap2();
		main.testMap3();
		main.testOptional("Appel moi demain");
		main.testIterate(4);
		main.testFindFirst();
		main.testCollect();
		main.testMap4();
		main.testStringReplace();
		main.testDigits();
		main.testMap5();
		main.testDigits2();
		main.getClassName(Fruit.class);
		main.testIntSummaryStatistic();
		main.testStringJoin();
		main.testDate();
		main.testFieldReflection();
		System.out.println("Hello World!");
		System.out.println(new BigDecimal("0.0000").stripTrailingZeros());*/
	}
}
