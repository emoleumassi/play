/*
 * Copyright 2017 IQTIG – Institut für Qualitätssicherung und Transparenz im Gesundheitswesen.
 * Diese Code ist urheberrechtlich geschützt (Copyright). Das Urheberrecht liegt, soweit nicht ausdrücklich anders gekennzeichnet, beim IQTIG.
 * Wer gegen das Urheberrecht verstößt, macht sich gem. § 106 ff Urhebergesetz strafbar. Er wird zudem kostenpflichtig abgemahnt und muss
 * Schadensersatz leisten.
 */

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author emo.leumassi
 */
public class TestField {

	private boolean active;
	private String Bezeichnung;
	private String WhatEver;
	private Integer lowerValue;
	private Integer upperValue;
	private Float percentile_95_Value;
	private String Id;


	TestField(final AtomicInteger fussnoteCounter) {
		System.out.println("TestField");
		System.out.println(fussnoteCounter.incrementAndGet());
	}
}
